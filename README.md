# OpenML dataset: Indeed-Software-Engineer-Job-Dataset

https://www.openml.org/d/43835

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This DataSet is scrapped from the Website https://www.Indeed.com/ , I want to scrapped this dataset, as I'm looking for a job and it comes to my mind what if I scapped all this 10000 dataset and analyze it.
Content
This Dataset contain 10,000 rows and 6 columns, 10000 rows means 10000 job profiles form different companies.
Acknowledgements
This DataSet is scrapped from Indeed Website, I want to thank them to allow me to scrap. Here is the Web Scrapping Code
https://www.kaggle.com/samrat77/indeed-job-web-scraping
Inspiration
Please analyze it if you want to, and if there is any mistake in the data, Please Feel free to correct me and also if you want to ask anything about the web scraping process, feel free to ask.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43835) of an [OpenML dataset](https://www.openml.org/d/43835). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43835/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43835/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43835/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

